﻿using System.ComponentModel.DataAnnotations;

namespace TCBlazorLib.Models
{
    public class UserCredentials
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
