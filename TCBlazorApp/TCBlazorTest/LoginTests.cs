using Bunit;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using TCBlazorLib.Components;

namespace TCBlazorTest
{
    public class LoginTests : TestContext
    {
        [Fact]
        public void Login_OnValidCredentials_ShouldRedirectToSuccessPage()
        {
            //arrange
            var nav = Services.GetRequiredService<NavigationManager>();

            var cut = RenderComponent<Login>();

            cut.Find("#LoginInput").Change("user");
            cut.Find("input[type=password]").Change("pass");

            // act
            cut.Find("#LoginBtn").Click();

            // assert
            Assert.Equal("http://localhost/SuccessfullLogin", nav.Uri);
        }

        [Fact]
        public void Login_OnInValidCredentials_ShouldRedirectToPageWithErrorMessage()
        {
            //arrange
            var nav = Services.GetRequiredService<NavigationManager>();

            var cut = RenderComponent<Login>();

            cut.Find("#LoginInput").Change("user");
            cut.Find("input[type=password]").Change("12345");

            // act
            cut.Find("#LoginBtn").Click();

            // assert
            Assert.Equal("http://localhost/Invalid Credentials", nav.Uri);
        }
    }
}